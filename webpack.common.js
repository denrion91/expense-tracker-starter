const path = require('path');

module.exports = {
  entry: { app: './src/assets/js/app.js' },
  output: {
    filename: 'assets/js/[name].[contentHash].bundle.js',
    path: path.resolve(__dirname, 'dist'),
  },
  module: {
    rules: [],
  },
};
